#!/bin/bash

# shellcheck source=common.sh
source "$(dirname "$0")/common.sh"

function assume-role() {
  [[ -z "$ROLE_ARN" ]] && return
  info "Assuming AWS IAM Role: $ROLE_ARN"
  session_name="$(echo -n "bitbucket-${BITBUCKET_REPO_FULL_NAME}-${BITBUCKET_BUILD_NUMBER}" | tr '[:upper:]' '[:lower:]' | tr -C '[:alnum:]' '-')"
  info "Role Session Name: $session_name"
  set +e
  json="$(aws sts assume-role --role-arn "$ROLE_ARN" --role-session-name "$session_name")"
  status=$?
  set -e
  [[ $status != "0" ]] && fail "Failed to assume IAM Role: $ROLE_ARN"
  AWS_ACCESS_KEY_ID="$(jq -r .Credentials.AccessKeyId <<< "$json")"
  AWS_SECRET_ACCESS_KEY="$(jq -r .Credentials.SecretAccessKey <<< "$json")"
  AWS_SESSION_TOKEN="$(jq -r .Credentials.SessionToken <<< "$json")"
  export AWS_ACCESS_KEY_ID
  export AWS_SECRET_ACCESS_KEY
  export AWS_SESSION_TOKEN
  success "Successfully assumed role: $ROLE_ARN"
}

assume-role

run $COMMAND
[[ $status != "0" ]] && fail "Failed to run command"
success "Success"
