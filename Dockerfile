FROM amazon/aws-cli:2.0.62

WORKDIR /aws

RUN yum install -y jq

COPY common.sh .
RUN chmod +x common.sh

COPY pipe.sh .
RUN chmod +x pipe.sh

ENTRYPOINT ["/aws/pipe.sh"]
